# GreenDAO
## 简介
>GreenDAO是一个具有一行代码操作数据库或链式调用,备份、升级、缓存等特性的关系映射数据库

## 效果展示
![效果展示](gif/sample.gif)

## 下载安装
```
npm install @ohos/greendao --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。


## 使用说明

1. 创建实体类，如Note
```
/**
 * Entity mapped to table "NOTE".
 */
import {Id} from '@ohos/greendao';
import {NotNull} from '@ohos/greendao';
import {Table, Columns} from '@ohos/greendao';
import {ColumnType} from '@ohos/greendao';

@Table('NOTE')
export  class Note {

  @Id()
  @Columns('ID', ColumnType.num)
  id: number;

  @NotNull()
  @Columns('TEXT', ColumnType.str)
  text: string;
  @Columns('COMMENT', ColumnType.str)
  comment: string;
  @Columns('DATE', ColumnType.str)
  date: Date;

  @Columns('TYPE', ColumnType.str)
  type: string;

   @Columns('MONEYS', ColumnType.real)
  moneys: number;

  //todo 类中必须在constructor中声明所有非静态变量，用于反射生成列
  constructor(id?: number, text?: string, comment?: string, date?: Date, types?: string,moneys?:number) {
    this.id = id;
    this.text = text;
    this.comment = comment;
    this.date = date;
    this.type = types;
    this.moneys=moneys;
  }
 getMoneys(): number {
    return  this.moneys;
  }

  setMoneys(moneys: number) {
    this.moneys = moneys;
  }
  getId(): number {
    return  this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getText(): string {
    return  this.text;
  }

  /** Not-null value; ensure this value is available before it is saved to the database. */
  setText(text: string) {
    this.text = text;
  }

  getComment(): string {
    return  this.comment;
  }

  setComment(comment: string) {
    this.comment = comment;
  }

  getDate(): Date {
    return  this.date;
  }

  setDate(date: Date) {
    this.date = date;
  }

  getType(): string{
    return this.type;
  }

  setType(types: string) {
    this.type = types;
  }

}
```


2. 非加密库在AbilityStage.ts进行初始化

 ```
 globalThis.contt = this.context;
 globalThis.entityCls = {}
 let helper: ExampleOpeHelper = new ExampleOpenHelper(this.context, "notes.db");
 let db: Database = await helper.getWritableDb();
 helper.setEntitys(Note);
 helper.onCreate_D(db);
 this.data.daoSession = new DaoMaster(db).newSession();
 ```

3. 加密库在AbilityStage.ets进行初始化

 ```
 globalThis.contt = this.context;
 globalThis.entityCls = {}
 let helper: ExampleOpenHelper = new ExampleOpenHelper(context, "notes.db");
 //设定数据加密密钥，加密后不可变更，加密和非加密库暂不能切换（普通数据库不能在设定为加密库，加密库不能变更为普通库，一经生成不可变更）
 //todo 注：参数必须为非空，否则会认定为非加密库（如：''或者""都为非加密库）
 helper.setEncryptKeyStr('encryptKey');
 let db: Database = await helper.getWritableDb();
 //将所有的表 (新增,修改,已存在)加到全局
 helper.setEntitys(Note);
 //调用创建表方法,将新增表创建,若无新增则不创建表
 helper.onCreate_D(db);
 this.data.daoSession = new DaoMaster(db).newSession();
 ```

4. 获取daoSession和Dao对象：在调用页面，如demo中的index页中

 ```
private aboutToAppear() {
    daoSess =  globalThis.daoSession;
    that.daoSession = daoSess;
    noteDaos = that.daoSession.getBaseDao(Note);
}
 ```

5.添加和移除监听

 ```
 /*
   *监听
   */
 private tabListener(): OnTableChangedListener<any>{
    let that = this;
    return {
      async onTableChanged(t: any, err, action: TableAction) {
        if (action == TableAction.INSERT) {          
          await that.updateNotes();
        } else if(action == TableAction.UPDATE){
          await that.updateNotes();
        } else if(action == TableAction.DELETE){
          await that.updateNotes();
        } else if (action == TableAction.QUERY) {
        }
      }
    }
  }
  /*
   *添加监听
   */
  noteDaos.addTableChangedListener(that.tabListener())；
  
  /**
   * 移除监听
   */
  noteDaos.removeTableChangedListener();
      
 ```

6.数据库操作

```
//新增
let date = new Date()
let comment = "Added on " + date.toLocaleString();

let note = new Note();
note.setText(this.noteText);
note.setComment(comment);
note.setDate(new Date());
note.setType(NoteType[NoteType.TEXT]);
noteDaos.insert(note);


//查询
let properties = globalThis.entityCls[Note.name];
let notesQuery = that.noteDao.queryBuilder().orderAsc(properties['text']).build();
this.arr = await this.notesQuery.list();


//删除
let properties = globalThis.entityCls[Note.name];
let deleteQuery = this.noteDao.queryBuilder().where(properties['text'].eq("bbb"))
  .buildDelete();
deleteQuery.executeDeleteWithoutDetachingEntities()
```

## 接口说明

``` 
接口准备
AbilityStage.ts
this.data.daoSession = new DaoMaster(db).newSession();
index.ets
let daoSess = globalThis.daoSession;
let noteDaos = that.daoSession.getBaseDao(Note)
let queryBuilder: QueryBuilder<Note> = this.noteDao.queryBuilder();
let migration =new Migration(this.dbName, this.tableName, 2).addColumn("AGE", ColumnType.realValue)
let properties = globalThis.entityCls[Note.name];
```

1. 新增
   `noteDao.insert(note)`
2. 修改 
   `noteDao.update(note)`
3. 删除 
   `noteDao.delete(note)`
4. 主键删除
   `noteDao.deleteByKey(id)`
5. 条件删除
   `noteDao.queryBuilder().where(properties['text'].eq("bbb")).buildDelete()`
6. 查询
   `noteDao.queryBuilder().list()`
7. 条件查询 
   `noteDao.queryBuilder.whereOr(properties['text'].eq("aaa"), properties['text'].eq("bbb"), properties['text'].eq("ccc")).list()`
8. 缓存查询
   `noteDao.load(id)`
9. 更新数据
   `noteDao.refresh(note)`
10. 链式查询
    `new qury().from(Note).query(Note).then((data) => { if(data)this.arr = data; })`
11. 链式条件查询
    `qury().from(Note).eq("ID", 2).querySingle(Note).then((data) => {if(data) this.arr = data; })`
12. 添加监听
    `noteDao.addTableChangedListener()`
13. 移除监听
    `noteDao.removeTableChangedListener()`
14. 保存(无则新增,有则更新)
    `noteDao.save(note)`
15. 备份数据库
    `Migration.backupDB(this.dbName)`
16. 升级数据库
    `migration.execute(context)`
17. 数据库从资源文件导入数据
    `executeSqlScript(resourceMgr: any, db: Database, rawFilename: string)`
18. 原始sql语句查询
    `rawQuerys(sql: string, selectionArgs: Array<any>)`


## 兼容性
支持 OpenHarmony API version 9 及以上版本。

## 目录结构
```
|---- greendao  
|     |---- entry  # 示例代码文件夹
|     |---- greenDao  # greenDao库文件夹
|               |----annotation # 注解相关
|               |----common # 公用类包
|               |----database # 数据库相关
|               |----dbflow # 链式查询
|                   |----base # 链式封装
|                   |----listener # 监听回调
|               |----identityscope # 缓存相关
|               |----internal # 内部调用文件
|               |----query # 查询
|           |---- index.ts  # 对外接口
|     |---- README.MD  # 安装使用方法
```

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/greenDAO/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/greenDAO/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/greenDAO/blob/master/LICENSE) ，请自由地享受和参与开源。