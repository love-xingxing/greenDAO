/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *   http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

import dataRdb from '@ohos.data.rdb';
import { StorageUtils } from '../StorageUtils'
import { Unit8ArrayUtils } from '../Unit8ArrayUtils'

export abstract class SQLiteOpenHelper {
    public constructor(context: any, name: string, version: number, factory?: any, errorHandler?: any) {
        this.dbName = name;
        this.context = context;
        this.version = version;
    }

    getDatabaseName(): string {
        return this.dbName;
    }

    async getWritableDatabase(): Promise<dataRdb.RdbStore> {
        let rdbStore = await this.getRdbStore();
        return rdbStore;
    }

    async getReadableDatabase(): Promise<dataRdb.RdbStore> {
        let rdbStore = await this.getRdbStore();
        return rdbStore;
    }

    abstract onCreate(db: dataRdb.RdbStore);

    abstract onUpgrade(db: dataRdb.RdbStore, oldVersion: number, newVersion: number);

    onOpen(db: dataRdb.RdbStore) {
    }

    context: any;
    dbPath: string ;
    dbName: string ;
    version: number = 1;
    cfg = undefined;
    encryptKey: Uint8Array = undefined;

    setEncryptKey(encryptKey: Uint8Array) {
        this.encryptKey = encryptKey;
    }

    setEncryptKeyStr(encryptKeyStr: string) {
        if (encryptKeyStr) {
            let encryptKey: Uint8Array = Unit8ArrayUtils.stringToUint8Array(encryptKeyStr);
            this.encryptKey = encryptKey;
        }
    }

    protected async getRdbStore(): Promise<dataRdb.RdbStore> {
        this.version = await StorageUtils.getValueByKey("dbVersion", 1);
        if (this.cfg == undefined) {
            if (this.encryptKey == undefined) {
                this.cfg = {
                    name: this.dbName
                }
            } else if (this.encryptKey != undefined || this.encryptKey.length > 0) {
                this.cfg = {
                    name: this.dbName,
                    encryptKey: this.encryptKey
                }
            }
        }
        let promise = dataRdb.getRdbStore(this.context, this.cfg, this.version);
        return promise;
    }

    protected getFileDir(): string{
        return this.context.filesDir;
    }
}
