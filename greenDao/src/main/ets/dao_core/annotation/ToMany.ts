/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

/**
 * Defines *-to-N relation
 */
import {JoinProperty, JoinPropertyEntity} from './JoinProperty'
class referencedJoinProperty{
  public readonly key: string = 'referencedJoinProperty';
  public value: string = '';
  constructor(value?: string){
    this.value = value;
  }
}
class joinProperties {
  public readonly key: string = 'joinProperties';
  public value: JoinPropertyEntity[];
  constructor(value: JoinPropertyEntity[] = [new JoinPropertyEntity('')]){
    this.value = value;
  }
};

export class ToManyEntity{
  referencedJoinPropertyParam: referencedJoinProperty;
  joinPropertiesParam: joinProperties;
  constructor(referencedJoinPropertyStr: string = '', entity: JoinPropertyEntity[] = [new JoinPropertyEntity()]){
    this.referencedJoinPropertyParam = new referencedJoinProperty(referencedJoinPropertyStr);
    this.joinPropertiesParam = new joinProperties(entity);
  }
}

export function ToMany(param: ToManyEntity = new ToManyEntity()){
  let referencedJoinPropertyTmp: referencedJoinProperty = param.referencedJoinPropertyParam;
  let joinPropertiesTmp: joinProperties = param.joinPropertiesParam;

  return (target, propertyKey) =>{
    Reflect.defineMetadata(referencedJoinPropertyTmp.key, referencedJoinPropertyTmp.value, target, propertyKey);

    for(let property of joinPropertiesTmp.value){
      let nameTmp = property.nameParam;
      let referencedNameTmp = property.referencedNameParam;

      Reflect.defineMetadata(nameTmp.key, nameTmp.value, target, propertyKey);
      Reflect.defineMetadata(referencedNameTmp.key, referencedNameTmp.value, target, propertyKey);
    }

  }
}
